# DesertCamel.BaseMicroservices.SuperBootstrap.Core

Fundamental NuGet Package for basic utilities & classes.

## Get Started

1. Install the NuGET Package [here](https://www.nuget.org/packages/DesertCamel.BaseMicroservices.SuperBootstrap.Core/).
