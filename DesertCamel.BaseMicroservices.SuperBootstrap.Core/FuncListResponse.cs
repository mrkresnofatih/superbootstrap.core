﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesertCamel.BaseMicroservices.SuperBootstrap.Core
{
    public class FuncListResponse<T>
    {
        public List<T> Data { get; set; }
        public long Total { get; set; }
        public string ErrorMessage { get; set; }

        public bool IsError()
        {
            return !String.IsNullOrWhiteSpace(ErrorMessage);
        }
    }
}
